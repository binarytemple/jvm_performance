object LoopConstructsStatsMain {

    def main(args:Array[String]) = {

        val Count = 1000000000

        {
            println("testing while loop")
            var start = System.currentTimeMillis();
            var total:Long = 0;
            var i=0;
            while(i < Count) { 
                i=i+1;
                total += 1
            };
            var end = System.currentTimeMillis();
            println(s"Time:${ end - start} ");
            println(total);
        }

        {
            println("testing for loop")
            var start = System.currentTimeMillis();
            var total:Long = 0;for(i <- 0l until Count) { total += 1 };
            var end = System.currentTimeMillis();
            println(s"Time:${end - start}");
            println(total);
        }

        {
            println("testing for loop - no closure ")
            val LocalCount = 1000000000
            var start = System.currentTimeMillis();
            var total:Long = 0;for(i <- 0l until LocalCount ) { total += 1 };
            var end = System.currentTimeMillis();
            println(s"Time:${end - start}");
            println(total);
        }


        {
            println("testing for range")
            var start = System.currentTimeMillis();
            var total:Long = 0;(1 to Count).foreach{i => total += 1 }
            var end = System.currentTimeMillis();
            println(s"Time:${end - start}");
            println(total);
        }

        {
            println("testing tailrec")
                @scala.annotation.tailrec
                def tailr(cur:Long,max:Long):Long = {
                    if(cur < max) {
                        tailr(cur + 1,max)
                    }else {
                        cur
                    }
                } 
            var start = System.currentTimeMillis();
            var total = tailr(0,Count) 
                var end = System.currentTimeMillis();
            println(s"Time:${end - start}");
            println(total);
        }

    }

}

