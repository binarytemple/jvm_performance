import org.apache.commons.lang.time.StopWatch
import java.util.{TimeZone, Calendar, Date}

object TimeStats extends App {

  val ITTER = 10000000

  def timeIt(title: String)(op: => Unit) = {
    val sw = new StopWatch()
    sw.start()
    op
    sw.stop()
    println("Time using %s : %s".format(title, sw.getTime))
  }

  for{ _ <- Range(0,5)}{
    timeIt("Date")(for {i <- Range(0, ITTER)} yield new Date())
    timeIt("System.currentTimeMillis")(for {i <- Range(0, ITTER)} yield System.currentTimeMillis())
    timeIt("Calendar.getInstance")(for {i <- Range(0, ITTER)} yield Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime)
  }
}
